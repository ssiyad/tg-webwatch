import pickle
import urllib.request
import hashlib
import random
from telegram.ext import CommandHandler, run_async, Updater

import config

updater = Updater(config.BOT_TOKEN, use_context=True)
dp = updater.dispatcher


def getHash(url):
    # random integer to select user agent
    randomint = random.randint(0,7)
    # User_Agents
    # This helps skirt a bit around servers that detect repeated requests from the same machine.
    # This will not prevent your IP from getting banned but will help a bit by pretending to be different browsers
    # and operating systems.
    user_agents = [
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
        'Opera/9.25 (Windows NT 5.1; U; en)',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.142 Safari/535.19',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:11.0) Gecko/20100101 Firefox/11.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:8.0.1) Gecko/20100101 Firefox/8.0.1',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.151 Safari/535.19'
    ]

    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent', user_agents[randomint])]
    response = opener.open(url)
    the_page = response.read()

    return the_page, hashlib.sha224(the_page).hexdigest()


@run_async
def core(update, context):
    if len(context.args) == 1:
        if True:  # Regex here
            msg = "No change"

            try:
                print(context.args[0])
                if "http://" in context.args[0] or "https://" in context.args[0]:
                    addr = context.args[0]
                else:
                    addr = "http://" + context.args[0]
                print(addr)
                remote_data, remote_hash = getHash(addr)
                local_data = pickle.load(open("urlinfo", "rb"))
                local_hash = hashlib.sha3_224(local_data).hexdigest()

                if local_hash != remote_hash:
                    msg = "Website changed!"
                else:
                    pickle.dump(remote_data, open("urlinfo", "wb"))

            except (ValueError, AttributeError):
                msg = Exception

            context.bot.send_message(chat_id=update.effective_chat.id, text=msg)

    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="More than one argument! Errr!")


def main():
    dp.add_handler(CommandHandler("check", core))
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
#
